import unittest
from org.barbarasudy.clustering.Clustering import Point, MainPoint, Initializer
filep = "resources/finalData.csv" 
var_l = ["G"]
v_plot = ["G", "A"]
k = 2
t = "Kmaxoid"
cl = Initializer(k_val = k, type_of_cl= t, filePath=filep, var_list=var_l, vars_plotting=v_plot).initializeClustering()

class TestMainPoints(unittest.TestCase):
        
    def testGetPoints(self):
        self.assertIsInstance(cl.point_list[0], Point)
        self.assertTrue(len(cl.point_list) > 2)
        
    def testGetMainPoints(self):
        self.assertIsInstance(cl.main_points[0], MainPoint)
        self.assertEqual(len(cl.main_points), k)
        new_main_points = [MainPoint(coordinates=(0,0), label="Bob"),MainPoint(coordinates=(3,4), label="Jerry")]
        cl.setMainPoints(new_main_points)
        self.assertEqual(cl.main_points[0].distanceFromOtherPoint(cl.main_points[1]), 5)
        
        self.assertEqual(cl.main_points[0].getCoordinates(), (0,0))
        self.assertEqual(cl.main_points[1].getLabel(), "Jerry")
        
    
        
if __name__ == "__main__" :
    unittest.main()