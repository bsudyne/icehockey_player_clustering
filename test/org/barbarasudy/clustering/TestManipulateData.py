import unittest
import numpy as np
from org.barbarasudy.clustering.ReadData import CSVReader
from org.barbarasudy.clustering.ManipulateData import Manipulate

class TestManipulateData(unittest.TestCase):
    global reader, df_num, df, manip
    filePath = "resources/750_players_.csv"
    reader = CSVReader(filePath)
    df = reader.getDataFrame()
    manip = Manipulate(df)
    
    def testReadData(self):
        self.assertEqual(manip.getPlayerNames()[0], "Wayne Gretzky")
        self.assertIsInstance(manip.manipulatePos()["Pos"][0], int)
        self.assertIsInstance(manip.manipulatePos()["Pos"][10], int)
        self.assertIsInstance(manip.manipulate_TOI()["TOI_GP"][0], float)
        df2_num = manip.getNumericData()
        self.assertEqual(np.all(np.isfinite(df2_num)), True)
        self.assertTrue(df2_num.isnull().values.any() == 0)
        
if __name__ == "__main__":
    unittest.main()