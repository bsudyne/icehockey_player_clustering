import unittest
from org.barbarasudy.clustering.Clustering import Point, MainPoint, Clustering_algorithm, Initializer

filep = "resources/finalData.csv" 
var_l = ["G","A"]
v_plot = ["G", "A"]
k = 2
t = "Kmaxoid"
cl = Initializer(k_val = k, type_of_cl= t, filePath=filep, var_list=var_l, vars_plotting=v_plot).initializeClustering()

class TestInitializer(unittest.TestCase):
    
    def testPointList(self):
        self.assertIsInstance(cl, Clustering_algorithm)
        self.assertTrue(len(cl.point_list)>0)
        self.assertIsInstance(cl.point_list[0], Point)
        self.assertEqual(len(cl.main_points), k)
        self.assertIsInstance(cl.main_points[0], MainPoint)
        
    
if __name__ == "__main__":
    unittest.main()