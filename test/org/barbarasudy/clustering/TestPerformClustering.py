import unittest
from org.barbarasudy.clustering.Clustering import Point
from org.barbarasudy.clustering.PerformClustering import PerformClustering

k = 2
player_names = ["Bob", "Bill", "Barnaby"]
coordinates_ = [(1,2,3,4), (11,123,44,2), (14,2,7,7)]
datapoints = [Point(coordinates = coordinates_[i], label = player_names[i]) for i in range(len(coordinates_)) ]

p = PerformClustering()

class TestPerformClustering(unittest.TestCase):
    
    def testPerformClustering(self):
        self.assertIsInstance(p.perform_clustering(), list) # main point list
        
if __name__ == "__main__":
    unittest.main()
        