import unittest
import numpy as np
import pandas as pd
from org.barbarasudy.clustering.ReadData import CSVReader

class TestReadData(unittest.TestCase):
    global reader, df, df_mx ,reader2
    filePath = "resources/testData.csv"
    filePath2 = "resources/750_players_.csv"
    reader = CSVReader(filePath)
    reader2 = CSVReader(filePath2)
    
    def testReadData(self):
        self.assertIsInstance(reader.getDataFrame(),pd.DataFrame)
        self.assertIsInstance(reader.getMatrixOfData(), np.ndarray)
        self.assertEqual(reader.getDataFrame().columns[0], "Player")
        self.assertEqual(reader.getDataFrame().columns[-1], "FOW_Perc")
        
        self.assertIsInstance(reader2.getDataFrame(),pd.DataFrame)
        self.assertIsInstance(reader2.getMatrixOfData(), np.ndarray)
        
        df2 = reader2.getDataFrame()
        self.assertEqual(df2.columns[0], "Player")
        self.assertEqual(df2.columns[-1], "FOW_Perc")
        
        
if __name__ == "__main__":
    unittest.main()