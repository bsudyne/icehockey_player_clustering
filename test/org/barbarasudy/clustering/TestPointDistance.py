import unittest
from org.barbarasudy.clustering.Clustering import Point

class TestPointDistance(unittest.TestCase):
    
    def test_point_distance(self):
        point1 = Point([0])
        point2 = Point([5])
        self.assertEqual(5, point1.distanceFromOtherPoint(point2))
        
        point1.updateCoordinates((0,0))
        point2 = Point((3,4))
        self.assertEqual(5, point1.distanceFromOtherPoint(point2))
        
        point1 = Point([0,1])
        point2 = Point([3,10])
        self.assertAlmostEqual(9.486, point1.distanceFromOtherPoint(point2), delta = 0.001)

if __name__ == '__main__':
    unittest.main()