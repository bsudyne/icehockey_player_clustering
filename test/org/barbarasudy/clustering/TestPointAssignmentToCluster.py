import unittest
from org.barbarasudy.clustering.Clustering import Point, MainPoint, Clustering_algorithm

class TestPointAssignment(unittest.TestCase):
    global c1, c2, p1, p2, p3, p4, p5, cluster_, list_of_clusters, cluster2_, list_of_clusters2
    p1, p2, p3, p4, p5 = Point((0,0)), Point((0,1)), Point((5,4)), Point((6,7)), Point((0,2))
    c1, c2, c3 = MainPoint((0,0)), MainPoint((6,7)), MainPoint((5,4))
    cluster_, cluster2_ = Clustering_algorithm(k=2, type_of_clustering="Kmean"), Clustering_algorithm(k=3, type_of_clustering="Kmean")
    cluster_.point_list, cluster2_.point_list = [p1,p2,p3,p4,p5], [p1,p2,p3,p4,p5]
    cluster_.setClusterCentroids([c1,c2]), cluster2_.setClusterCentroids([c1,c2, c3])
    
    def testPointAssignment_clusterIndex(self):
        list_of_clusters = cluster_.calculatePointIdxToCluster()
        self.assertIsInstance(list_of_clusters, list)
        # does it return with the correct indices of the cluster's point list?
        self.assertEqual(list_of_clusters[0], [0,1,4])
        self.assertEqual(list_of_clusters[1], [2,3])
        
        list_of_clusters2 = cluster2_.calculatePointIdxToCluster()
        self.assertIsInstance(list_of_clusters2, list)
        # does it return with the correct indices of the cluster's point list?
        self.assertEqual(list_of_clusters2[0], [0,1,4])
        self.assertEqual(list_of_clusters2[1], [3])
        self.assertEqual(list_of_clusters2[2], [2])
    
    def testAssignmentMx(self):
        assignmentMx = cluster_.getAssignmentMatrix()
        self.assertEqual(assignmentMx[0,2], 0)
        self.assertEqual(assignmentMx[0,4], 1)
        self.assertEqual(assignmentMx[1,2], 1)
        self.assertEqual(assignmentMx[1,4], 0)
    

if __name__ == "__main__":
    unittest.main()