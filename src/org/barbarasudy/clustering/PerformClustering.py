from org.barbarasudy.clustering.Clustering import Initializer
import numpy as np

class PerformClustering(object):
    
    def __init__(self, k_ , type_of_clustering, file_Path, varList, varsPlotting):
        self.k_ = k_
        self.type_of_clustering = type_of_clustering
        self.file_Path = file_Path
        self.varList = varList
        self.varsPlotting = varsPlotting
    
    def perform_clustering(self):
        c = Initializer(k_val = self.k_ , type_of_cl = self.type_of_clustering, filePath = self.file_Path, var_list = self.varList, vars_plotting = self.varsPlotting).initializeClustering()
        print(c.main_points, c.point_list)
        needMoreUpdate = 1
        
        while(needMoreUpdate):
            c.recalculateMainPoint()
            needMoreUpdate = c.update_clusters()
    
        for i in range(c.k):
            print("Main point of cluster ", i, " is : ", c.main_points[i].label)
            print("Cluster ", i, " includes:")
            num_of_cluster_elements = 0
            for j in c.point_list:
                if(j.cluster_number == i):
                    print(j)
                    num_of_cluster_elements += 1
            print("Elements in cluster: ", num_of_cluster_elements)
            print()
        ### Plotting results    
        def plot(vars_to_plot = []):
            from mpl_toolkits.mplot3d import Axes3D, proj3d
            import matplotlib.pyplot as plt
            
            fig = plt.figure()
            ax = fig.add_subplot(111)
            xs = c.plotting_points[0]
            ys = c.plotting_points[1]
            mp_x = [c.main_points[i].getCoordinates()[0] for i in range(c.k)]
            mp_y = [c.main_points[i].getCoordinates()[1] for i in range(c.k)]
            c_i = c.calculateClusterIdxToPoints()
            if np.shape(c.plotting_points)[0] == 3:
                
                zs = c.plotting_points[2]
                mp_z = [c.main_points[i].getCoordinates()[2] for i in range(c.k)]
                zdir = (1,1,0)
                ax = fig.gca(projection='3d')
                ax.scatter(mp_x, mp_y, mp_z, c="black", s = 60, marker = "o")
                ax.scatter(xs, ys, zs, c=c_i, marker="x", s=30)
                #set label to z axis
                ax.set_zlabel(vars_to_plot[2])
                
                #annotation
                for i in range(c.k):
                    if c.type_of_clustering == "Kmean":
                        text = "C " + str(i+1)
                    else:
                        text = c.main_points[i].label
                        
                    x2, y2, _ = proj3d.proj_transform(mp_x,mp_y,mp_z, ax.get_proj())
                    ax.text(c.main_points[i].getCoordinates()[0],c.main_points[i].getCoordinates()[1],c.main_points[i].getCoordinates()[2], text, zdir, ha = 'right', va = 'top',
                            fontsize=10, color="black", weight = "bold")
                    
            else:
                for i in range(c.k):
                    if c.type_of_clustering == "Kmean":
                        text = "C " + str(i+1)
                    else:
                        text = c.main_points[i].label
                    ax.scatter(xs, ys, c=c_i, marker="x", s = 30)
                    ax.scatter(c.main_points[i].getCoordinates()[0],c.main_points[i].getCoordinates()[1], marker = "o", s = 40, c = "black")
                    ax.annotate(text, xy=(c.main_points[i].getCoordinates()[0],c.main_points[i].getCoordinates()[1]),
                     xytext=(-10, +30), textcoords='offset points', fontsize=15, color="black", weight = "bold",
                     arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=-0.6"))
            #Set label to x and y axis
            ax.set_xlabel(vars_to_plot[0])
            ax.set_ylabel(vars_to_plot[1])
            plt.show()
            
        plot(vars_to_plot=self.varsPlotting)
        return c.main_points
