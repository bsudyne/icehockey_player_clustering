from sklearn.preprocessing import MinMaxScaler
from org.barbarasudy.clustering.ReadData import CSVReader
import numpy as np
import random

class Point(object):
    
    def __init__(self, coordinates, label = None):
        self.coordinates = tuple(coordinates)
        self.label = label
        self.cluster_number = None
        
    def __repr__(self):
        return "Name of the data point: " + str(self.label) + ". Current cluster: " +  str(self.cluster_number) + " Coordinates are: " + str(self.coordinates)
    
    def getCoordinates(self):
        return self.coordinates
        
    def updateCoordinates(self, coordinates):
        self.coordinates = coordinates
    
    def distanceFromOtherPoint(self, other_point):
        # other point may be a Point or a Centroid
        assert len(self.coordinates) == len(other_point.coordinates)
        # Calculate distance of one point to another
        coo1 = np.array(self.coordinates)
        coo2 = np.array(other_point.coordinates)  
        return np.linalg.norm(coo1 - coo2)
    
class MainPoint(Point):
    """"Cluster cnetroids or maxoids"""
        
    def __init__(self, coordinates, label = None):
        Point.__init__(self, coordinates, label)
    
    def __repr__(self):
        return str(self.coordinates)
    
    def getLabel(self):
        return self.label
    
class Clustering_algorithm(object):
    
    def __init__(self):
        self.k = int
        self.type_of_clustering = "Kmean"
        self.point_list = []
        self.main_points = []
        self.plotting_points = []
    
    def setMainPoints(self, list_of_main_points):
        """In case cluster main points must be manually changed"""
        set_of_main_points = set(list_of_main_points)
        print(set_of_main_points)
        if len(set_of_main_points) != len(list_of_main_points):
            print("Maint points must differ in at least one coordinate!")
            return
        else:
            self.main_points = list_of_main_points
    
    def calculateClusterIdxToPoints(self):
        """For each point calculate which cluster they belong to. Returns an index list."""
        dist_list = []
        for main_p in self.main_points:
            temp_list = [point.distanceFromOtherPoint(main_p) for point in self.point_list]
            dist_list.append(temp_list)
        # Convert to np array and find the cluster index for each point where distanceFromOtherPoint is smallest
        dist_array = np.array(dist_list)
        cluster_idx_list = np.argmin(dist_array, axis=0)
        # Update cluster_number of points
        assert len(self.point_list) == len(cluster_idx_list)
        for i in range(len(self.point_list)):
            self.point_list[i].cluster_number = cluster_idx_list[i]
        return cluster_idx_list
    
    def calculatePointIdxToCluster(self):
        cluster_idx_list = self.calculateClusterIdxToPoints()           
        # Each cluster gets a list with the point indices
        idx_list = [list(np.where(cluster_idx_list == value)[0]) for value in range(self.k_)]
        print("Points to cluster: ", idx_list)
        return idx_list
    
    def recalculateMainPoint(self):
        if self.type_of_clustering == "Kmean":
            self.recalculateCentroid()
        elif self.type_of_clustering == "Kmaxoid":
            self.recalculateMaxoid()
            
    def recalculateCentroid(self):
        import operator
        print("Recalculate centroids....")
        i = len(self.point_list[0].getCoordinates())
        print("Cluster number, num of players in cluster, cluster centroid: ")
        for j in range(self.k):
            total_coos = tuple(np.zeros(i))
            totalInCluster = 0
            for p in self.point_list:
                if(p.cluster_number == j):
                    total_coos = tuple(map(operator.add, total_coos, p.getCoordinates()))
                    totalInCluster += 1
            print( j, totalInCluster, total_coos, p.getCoordinates())
            if(totalInCluster > 0):
                self.main_points[j].updateCoordinates(tuple(coo/totalInCluster for coo in total_coos))
        print("....DONE")
        
    def recalculateMaxoid(self):
        print("Recalculate maxoids....")
        max_dlist = []
        p_list = []
        copy_points = [pp for pp in self.point_list]
        # In each cluster check what's the farthest point from the other cluster's maxoids.
        # Make this the new maxoid of the cluster.
        for i in range(len(self.main_points)):
            copy_maxoids = [m for m in self.main_points]
            copy_maxoids.pop(i)
            max_dist = 0
            max_point = None
            print(self.main_points)
            for l in copy_maxoids:
                for j in copy_points:
                    if j.cluster_number == i:
                        dist = j.distanceFromOtherPoint(l)
                        if dist > max_dist:
                            max_dist = dist
                            max_point = j
                            
            print("Maxpoint.label: ", max_point.label)
            max_dlist.append(max_dist)
            p_list.append(max_point)
            copy_points.remove(max_point)  
        print([p.label for p in p_list], max_dlist)
        self.main_points = p_list
        print(".... DONE")
        
    def update_clusters(self):
        print("Updating clusters....")
        k = self.k
        large_num = 100000000
        needMoreUpdates = 0
         
        for p in self.point_list:
            best_minimum_distance = large_num
            currentCluster = None
             
            for j in range(k):
                distance = self.main_points[j].distanceFromOtherPoint(p)
                if(distance < best_minimum_distance):
                    best_minimum_distance = distance
                    currentCluster = j
                    
            if(p.cluster_number != currentCluster):
                print(p.label, p.cluster_number, '-------->' , currentCluster)
                p.cluster_number = currentCluster
                needMoreUpdates = 1
        print("Still updating? --- ", needMoreUpdates)
        print("....DONE")
        return needMoreUpdates
        
class Initializer(object):
    
    def __init__(self, k_val, type_of_cl, filePath, var_list, vars_plotting):
        self.k_val = k_val
        self.type_of_cl = type_of_cl
        self.filePath = filePath
        self.var_list = var_list
        self.vars_plotting = vars_plotting
        
    def initializeClustering(self):
        clustering_alg = Clustering_algorithm()
        clustering_alg.k = self.k_val
        clustering_alg.type_of_clustering = self.type_of_cl
        reader = CSVReader(self.filePath)
        
        def sci_minmax(X):
            minmax_scale = MinMaxScaler(feature_range=(0, 10), copy=True)
            return minmax_scale.fit_transform(X)
        
        data_matrix = reader.getMatrixOfData()
        if isinstance(data_matrix[0,0], int):
            data_matrix = data_matrix[:,1:]
        player_names = data_matrix[:,0]
        
        print("Initialize data points....")
        # Create data points with scaled coordinates
        coordinates_ = sci_minmax(reader.getNumericData()[self.var_list])
        datapoints = [Point(coordinates = coordinates_[i], label = player_names[i]) for i in range(len(coordinates_))]
        clustering_alg.point_list = datapoints
        print("DONE")
        
        print("    Initialize points to plot....")
        to_plot = sci_minmax(reader.getNumericData()[self.vars_plotting])
        to_plot_list = [list(cols) for cols in to_plot.T]
        clustering_alg.plotting_points = to_plot_list
        print("    ....DONE")

        def initializeMainPoints(): 
            """Initialise centroids or maxoids as random chosen data points from the data set"""
            print("Initializing main points....")
            point_list = clustering_alg.point_list
            k = self.k_val
            chosen_points = [point_list[i] for i in random.sample(range(len(point_list)), k)]
            coordinate_list = [chosen_points[i].getCoordinates() for i in range(len(chosen_points))]            
            clustering_alg.main_points = [MainPoint(coordinate) for coordinate in coordinate_list]
            for i in range(len(clustering_alg.main_points)):
                clustering_alg.main_points[i].label = i
        
        initializeMainPoints()
        print("....DONE")
        clustering_alg.calculateClusterIdxToPoints()
        
        return clustering_alg
