import pandas as pd
import numpy as np

class CSVReader(object):
    
    def __init__(self,filepath):
        self.filepath = filepath
    
    def getDataFrame(self):
        ## reading csv into dataframe
        df = pd.read_csv(self.filepath, header=0, low_memory=False)
        return df
    
    def getMatrixOfData(self):
        mx = self.getDataFrame().as_matrix(columns = None)     ## reading csv into array
        return mx
    
    
    def nonAsciiReplace(self, var, replaceWith = ''):
        '''Replace non ASCII strings in variables.'''
        
        for i in range(0,len(var)):
            try:
                var[i].decode('ascii')
            except UnicodeDecodeError:
                var[i] = replaceWith
        return var
    
    def getNumericData(self):
        df = self.getDataFrame()
        def is_number(s):
            try:
                float(s)
                return True
            except ValueError:
                return False    
        
        numeric = []
        for i in range(np.shape(df)[1]):
            if is_number(df.ix[0,i]):
                numeric.append(df[[i]])
        df = pd.concat(numeric, axis = 1)
        df = df.fillna(df.mean()).astype(float)
        #print("Mean values: " , df.mean())
        return df # returns the dataFrame of numeric variables
    
    
        