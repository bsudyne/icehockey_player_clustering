from org.barbarasudy.clustering.ReadData import CSVReader
from org.barbarasudy.clustering.ManipulateData import Manipulate
import pandas as pd

class PerformDataManipulation(object):
    
    def __init__(self, filePath, csvName, size_of_data = None):
        self.filePath = filePath
        self.csvName = csvName
        self.size_of_data = size_of_data
    
    def main(self):
        reader = CSVReader(self.filePath)
        df = reader.getDataFrame()
        if self.size_of_data:
            df = df.ix[:int(self.size_of_data)-1,:]
        print(df)
        m = Manipulate(df)
        for stat in ["G", "A", "P", "PIM", "S", "Plus_minus"]:
            m.calculate_stat_GP(stat)
        df = m.manipulate_TOI()
        df = m.manipulatePos()
        print(df)
        #m.writeToCsv("finalData_added")
        print("DONE")
    

fileP = "resources/active.csv"
csv = "finalData_added"
p = PerformDataManipulation(filePath=fileP, csvName=csv, size_of_data=500)
p.main()