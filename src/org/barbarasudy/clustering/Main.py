from org.barbarasudy.clustering.PerformClustering import PerformClustering

filep = "resources/finalData_300_added.csv" #"resources/active_final.csv" 
# Player,Pos,Cur. Team,GP,G,A,P,Plus_minus,PIM,G_GP,A_GP,P_GP,PIM_GP,S_GP,Plus_minus_GP,PPG,PPP,SHG,SHP,GWG,OTG,S,S_Perc,TOI_GP,Shifts_GP,FOW_Perc,
var_l = ["P_GP","S_GP","PIM_GP", "A_GP", "G_GP"]
v_plot = ["P_GP","S_GP","PIM_GP"]
k = 3
t = "Kmaxoid"#"Kmean"#

p = PerformClustering(k_ = k, type_of_clustering = t, file_Path = filep, varList = var_l, varsPlotting = v_plot)
p.perform_clustering()

