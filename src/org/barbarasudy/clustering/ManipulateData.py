import numpy as np
import pandas as pd
from org.barbarasudy.clustering.ReadData import CSVReader

class Manipulate(object):
    
    def __init__(self, dataFrame):
        self.dataFrame = dataFrame

    def getPlayerNames(self):
        return self.dataFrame["Player"]
    
    def is_number(self,s):
            try:
                float(s)
                return True
            except ValueError:
                return False   
    
    def manipulate_TOI(self):
        import re
        x = []   
        
        for t in self.dataFrame["TOI_GP"]:
            # check if not Nan
            if pd.isnull(t) == False and not self.is_number(t):
                # count secs
                #print(t)
                times = map(int, re.split(r"[:,]", t))
                x.append(times[0]*60+times[1])
            else:
                x.append(0)
        df = self.dataFrame
        df["TOI_GP"] = x
        df.loc[df['TOI_GP'] == 0 , 'TOI_GP'] = df["TOI_GP"].mean()
        return df
        
    def manipulatePos(self):
        df = self.dataFrame
        for i in range(len(df)):
            if df["Pos"][i] == "C":
                df["Pos"][i] = 1  
            elif df["Pos"][i] == "L":
                df["Pos"][i] = 2
            elif df["Pos"][i] == "R":
                df["Pos"][i] = 3
            elif df["Pos"][i] == "D":
                df["Pos"][i] = 4
            else:
                df["Pos"][i] = 5
        return df
    
    def getNumericData(self):
        df = self.dataFrame   
            
        numeric = []
         
        for i in range(np.shape(df)[1]):
            if self.is_number(df.ix[0,i]):
                numeric.append(df[[i]])
        df = pd.concat(numeric, axis = 1)
        df = df.fillna(df.mean()).astype(float)
        print("Mean values: " , df.mean())
        return df # returns the dataFrame of numeric variables
        
    def writeToCsv(self, filaName):
        self.dataFrame.to_csv("resources/" + filaName + ".csv", index_col=False)
        
    def calculate_stat_GP(self, stat):
        stat = str(stat)
        self.dataFrame[stat + "_GP"] = self.dataFrame[stat]/self.dataFrame['GP']


